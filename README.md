Nutzung des TU Clusters
=======================

Zugang zum Cluster
------------------

Das Antragsformular von der Seite des Rechenzentrums muss ausgefüllt und
abgegeben werden.
Es kann unter dem Punkt "Rechnerzugang - Benutzerkennung" auf der folgenden
Seite gefunden werden: <http://bit.ly/cluster-hw>


Login auf dem Loginknoten
-------------------------

Es kann nur innerhalb des TU Netzes auf die Loginkoten zugegriffen werden.
Soll von außerhalb mit dem Cluster gearbeitet werden, muss zuerst eine VPN
Verbindung aufgebaut werden.

Der Zugriff auf den Loginknoten erfolgt per SSH.
Hierzu wird eine Nutzerkennung benötigt, die mit den entsprechenden
Berechtigungen ausgestattet ist. (Siehe "Zugang zum Cluster")


### Linux/Mac

- Ein Terminal öffnen.
- `ssh nutzerkennung@hpc5.rz.tu-harburg.de` eingeben.
- Es sollte nach einem Passwort gefragt werden. Hier das Passwort, welches zu
  der genutzten Nutzerkennung gehört eintragen.
- Die SSH Verbindung mit dem Cluster sollte jetzt funktionieren.


### Windows

- Die passende `putty.exe` hier herunterladen:
  http://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
- `putty.exe` starten.
- `nutzerkennung@hpc5.rz.tu-harburg.de` in das Feld
  `Host Name(or IP address)` eintragen.
- Unten rechts auf `Open` klicken.
- Es sollte nach einem Passwort gefragt werden. Hier das Passwort, welches zu
  der genutzten Nutzerkennung gehört eintragen.
- Die SSH Verbindung mit dem Cluster sollte jetzt funktionieren.
- Die Einstellung kann gespeichert werden, damit nicht jedes mal wieder der
  Name eingegeben werden muss.

In beiden Fällen muss `nutzerkennung` durch die Kennung ersetzt werden, die
vom Rechenzentrum für die Nutzung mit dem Cluster freigeschaltet wurde.

Manchmal wird nach dem Verbinden eine Seite mit Informationen angezeigt. Diese
kann mit einem Druck der Leertaste geschlossen werden.

Es gibt aktuell neben `hpc5`noch 4 weitere Loginknoten.
Generell kann ein beliebiger Rechenknoten genutzt werden, hpc5 ist allerdings
der aktuellste und zur Zeit der einzige auf dem CentOS 7 läuft.
Eine Lister aller Knoten kann auf der Hardwareseite des Rechenzentrums gefunden
werden: <http://bit.ly/cluster-hw>


Dateitransfer
-------------

Dateien können entweder in das eigenen Homeverzeichnis der TU gelegt werden oder
in das Dateisystem des Clusters. Dieses ist unter dem Pfad `/wt` zu finden.
Es ist zwar möglich, die lokalen Festplatten der Rechenknoten direkt zu nutzen,
der Einfachheit halber wird in dieser Anleitung aber das von überall zugängliche
Dateisystem des Clusters genutzt.

`/wt` ist eine Abkürzung zu dem eigentlichen Verzeichnis, welches unter
`/work/<gruppe>/<id>` zu finden ist. Mit dem Befehl `id` kann man seine id und
Gruppe herausfinden.


### Linux/Mac

Hochladen einer einzelnen Datei:

- Ein Terminal öffnen.
- `scp dateiname nutzerkennung@hpc5.rz.tu-harburg.de:wt/cluster_job`
  eingeben. `dateiname` durch den Namen der Datei ersetzen, die hochgeladen
  werden soll. `nutzerkennung` durch die vom Rechenzentrum freigeschaltete
  Kennung ersetzen.
- Das Passende Passwort eingeben.
- Die Datei wird nun in das `/wt/cluster_job` Verzeichnis hochgeladen. Der
Pfad kann bei Bedarf geändert werden.

Herunterladen einer einzelnen Datei:

- Ein Terminal öffnen.
- `scp nutzerkennung@hpc5.rz.tu-harburg.de:wt/cluster_job/dateiname /pfad/zum/ablegen`
  eingeben. `dateiname` durch den Namen der Datei ersetzen, die hochgeladen
  werden soll. `nutzerkennung` durch die vom Rechenzentrum freigeschaltete
  Kennung ersetzen. `/pfad/zum/ablegen` durch das Verzeichnis ersetzen, in dem
  die Datei abgelegt werden soll.
- Die Datei wird nun aus dem `/wt/cluster_job` Verzeichnis in den `/pfad/zum/ablegen`
  heruntergeladen.

Das Hoch- und Herunterladen von Ordnern funktioniert ähnlich wie für einzelne
Dateien. Einzige Änderung ist die Nutzung des `-r` Flags. Anstelle eines
Dateinamens wird der Name des Ordners genutzt.

- Hochladen: `scp -r ordnername nutzerkennung@hpc5.rz.tu-harburg.de:wt/cluster_job`
- Herunterladen: `scp -r nutzerkennung@hpc5.rz.tu-harburg.de:wt/cluster_job/ordnername /pfad/zum/ablegen`

Weitere Beispiele zur Nutzung von scp können hier gefunden werden:
<http://bit.ly/scp-help>


### Windows

- WinSCP hier herunterladen und gegebenenfalls installieren:
  <http://bit.ly/win-scp>
- WinSCP starten
- File Protocoll auf `SCP` stellen.
- `hpc5.rz.tu-harburg.de` als host name eintragen.
- Für `User Name` und `Password` die Nutzerkenung und das dazugehörige Passwort
  vom Rechententrum eintragen.
- Links sind die Daten auf dem eigenen Rechner zu sehen, rechts die auf dem
  Server.
- Auf dem Server in den Ordner mit dem Namen `wt` wechseln und dort einen neuen
  Ordner mit dem Namen `cluster_job` erstellen.
- Die Daten per drag and drop vom eigenen Rechner in den erstellten Ordner
  ziehen und den Dialog mit `OK` bestätigen.
- Zum Herunterladen die Datein per drag and drop vom Cluster auf den eigenen
  Rechner ziehen.


Nutzung des Clusters
--------------------

Normalerweise werden zwei Arten von Dateien auf den Cluster geladen werden
müssen:

- Dateien die analysiert oder bearbeitet werden sollen
- Programme, beziehungsweise Skripte, die auf die Dateien angewandt werden
  sollen.

Ist beides vorhanden, muss zuerst sichergestellt werden, ob der Cluster die
benötigten Abhängigkeiten für das auszuführende Programm bereitstellt.
Hierfür kann mithilfe des Befehls `module avail` auf dem Loginknoten eine Liste
der bereitgestellten Module angezeigt werden.
Falls unklar ist, ob alle Abhängigkeiten erfüllt sind, sollte der Kontakt mit
Rechenzentrum gesucht werden. Die Ansprechpartner für den Cluster sind hier zu
finden: <http://bit.ly/cluster-contact>

Falls alle Abhängigkeiten vorhanden sind, oder nicht bekannt ist ob noch etwas
fehlt, kann mit dem Schreiben des Auftrages begonnen werden.

### SLURM sbatch Skript

Um dem Cluster mitzuteilen, was genau er tun soll, muss ein Auftrag geschrieben
werden. Die Dokumentation des Rechenzentrums, welche auch einige Beispiele
enthält ist hier zu finden: <http://bit.ly/cluster-nutzung>

Hierfür muss ein Bash Skript erstellt werden.
Für diese Anleitung nennen wir es `job.sh`.

Um das fertige Skript an den Cluster zu übergeben wird der Befehl
`sbatch job.sh` benutzt.

Das Script besteht aus fünf Teilen:


#### Initialisierung

Die Initialisierung beinhaltet das anfordern von Hardware Ressourcen und andere
Voreinstellungen.

```
    #!/bin/bash
    #SBATCH -p ether
    #SBATCH --ntasks 1
    #SBATCH --cpus-per-task 1
    #SBATCH --time=12:00:00
    #SBATCH --constraint OS7
```

Solange das auszuführende Programm nicht auf verteiltes Rechnen auf mehreren
Rechnern ausgelegt ist oder Berechnungen auf Grafikkarten durchgeführt werden
sollen, sollte ` -p ether` und `--ntasks 1` gewählt werden.

Wenn das Programm mehrere CPU-Kerne nutzen kann, sollte `--cpus-per-task`
entsprechend angepasst werden.

Die Zeit wird im Format `hh:mm:ss` angegeben und sollte großzügig geschätzt
werden.

`--constraint OS7` zwingt den Cluster dazu, einen Rechenknoten mit CentOS 7 zu
nutzen. Dies ist manchmal nötig, da einige Abhängigkeiten unter CentOS 6 nur
veraltet vorhanden sind.

Eine Liste mit allen Befehlen und Erklärungen gibt es auf der Seite des
Rechenzentrums.


#### Module Laden

Installierte Softwaremodule können geladen werden um vom Programm genutzt werden
zu können. Hier muss darauf geachtet werden, dass manche Module in mehreren
Versionen vorliegen. Die Liste aller installierten Module kann mit dem Befehl
`module avail` auf dem Loginknoten angezeigt werden.

```
    . /etc/profile.d/module.sh
    module load r/3.3.2
    module load java/1.5.0
```

Die erste Zeile startet das Modulsystem, die zweite lädt das Modul für die R
Version 3.3.2
Die zweite Zeile lädt das Java Modul.


#### Vorbereiten der Daten

Falls die Daten nicht bereits im `/wt` Verzeichnis liegen, müssen sie noch
für die Rechenknoten zugänglich gemacht werden.
In dieser Anleitung wird davon ausgegangen, dass die Dateien im Pfad
`/wt/cluster_job` liegen, wie im Kapitel zum Hochladen von Datein beschrieben.

Beispiele zum Verschieben der Dateien können in den Beispielen des
Rechenzentrums gefunden werden.

In jedem Fall muss das Skript in den Ordner mit dem auszuführenden Programm
gebracht werden, in diesem Fall `/wt/cluster_job`.
Dies geschieht mit dem `cd` Befehl:

```
    cd ~/wt/cluster_job
```


#### Ausführen der Berechnung

Nachdem die Dateien für den Rechenknoten zugänglich sind, können die
Berechnungen durchgeführt werden.

```
    mkdir Rlibs
    export R_LIBS=~/wt/cluster_job/Rlibs
    R CMD javareconf -e
    R CMD BATCH script.R
```

In diesem Beispiel wird erst ein Ordner erstellt in dem R Pakete installiert
werden sollen. Der `export R_LIBS=$MYWORKDIR/Rlibs` Befehl lässt R auch in
diesem Ordner nach installierten Paketen suchen. Dies ist notwendig, da man
standardmäßig keine Schreibberechtigung für den eigentlichen Ordner hat.
Beim installieren von Paketen muss man dann darauf achten `lib="Rlibs/` zu setzen:  
`install.packages("devtools", repos="http://cran.r-project.org", lib="Rlibs/" , dependencies=TRUE)`

Die Zeile `R CMD javareconf -e` ermöglicht es Pakete zu nutzen, die auf Java
angewiesen sind.

Die Dritte Zeile führt das R script mit dem Namen `script.R`aus.
Es eine Datei mit dem namen `script.Rout` erstellt, in der die ausgaben des
Programms stehen, welche bei interaktivem Ausführen in der Konsole zu sehen
wären.

Nach dem Starten des Skriptes kann mit dem Befehl `squeue` die Warteschlange
des Clusters angezeigt werden.


##### Nachbereitung der Berechnung

Falls die Daten in ein anderes Verzeichnis gespeichert werden sollen oder direkt
Daten gelöscht werden können, kann dies in diesem Schritt passieren. Auch hier
finden sich wieder Anleitungen in den Beispielen des Rechenzentrums.
Für diese Anleitung ist das nicht nötig, da wir die Daten direkt auf den Cluster
gelegt haben und sie auch direkt von da wieder herunterladen können.

Zuletzt wird das Script beendet.

```
    exit
```

### Vollständiges Beispiel Skript für R

Dieses Skript unterstützt ein R Skript, dass Java benutzt und eigene Pakete
installiert.

```
#!/bin/bash
#SBATCH -p ether
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 1
#SBATCH --time=01:00:00
#SBATCH --constraint OS7

. /etc/profile.d/module.sh
module load r/3.3.2
module load java/1.5.0

cd ~/wt/cluster_job

mkdir Rlibs
export R_LIBS=~/wt/cluster_job/Rlibs
R CMD javareconf -e
R CMD BATCH script.R

exit
```

Vollständiger Ablauf eines Auftrags
-----------------------------------

1. Einen Account für den Cluster freischalten lassen.
2. Das auszuführende Programm und die benötigten Daten auf dem eigenen Rechner
   vorbereiten und auf den Cluster hochladen.
3. Das Auftragsskript entweder auf dem eigenen Rechner vorbereiten und auf den
   Cluster hochladen oder direkt auf dem Cluster erstellen.
4. Den Auftrag mit `sbatch job.sh` starten.
5. Nachdem der Auftrag beendet ist, die Ergebnisse entweder direkt auf dem
   Cluster betrachten oder auf den eigenen Rechner herunterladen.
6. Die nicht mehr benötigten Daten auf dem Cluster löschen.


Hilfreiche Links
----------------

Ansprechpartner des RZ: <http://bit.ly/cluster-contact>

Hardware Dokumentation des RZ: <http://bit.ly/cluster-hw>

Nutzungsdokumentation des RZ: <http://bit.ly/cluster-nutzung>

Hilfe mit scp: <http://bit.ly/scp-help>

Hilfe mit Unix Kommandos: <http://bit.ly/unix-help>
