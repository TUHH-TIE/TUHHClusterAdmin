#!/bin/Bash
#SBATCH -p ether
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 1
#SBATCH --time=00:10:00
#SBATCH --constraint OS7

#Loads R and Java module
. /etc/profile.d/module.sh
module load r/3.3.2
module load java/1.5.0

#Moves to the /wt/cluster_job directory
cd /wt/cluster_job

# Creates a Rlibs directory and tells R to look for installed packages in it
# This is necessary as you usually have no write access on the cluster and
# can't install packages in the default folder due to this.
mkdir Rlibs
export R_LIBS=/wt/cluster_job/Rlibs
# Configures Java to be usable by R
R CMD javareconf -e
#Runs the script.R file
R CMD BATCH script.R

exit