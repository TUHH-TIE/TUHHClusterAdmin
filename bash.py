#!/usr/bin/env python3

"""
This script creates a bash file for the high performance cluster of the TUHH.
For full documentation see https://www.tuhh.de/rzt/tuinfo/ausorg/hpc/

Say you want to run your python script calc.py on the cluster.
You can create the job script simply with:

    $ python bash.py 'python calc.py' --user_group <grp> --user_id <id>

The script will copy the cluster_job directory from your home directory into a
local working directory and write the results back into the cluster_results
directory in your home directory. It reserves one task on one cpu core on the
ether partition for an hour.
Use the 'id' comand to get your user group and id.
"""

import argparse


def write_sbatch(arguments, file):
    """
    This function writes the sbatch options to the bash file.

    :param arguments: The argparse arguments
    :param file: The bash file that is written
    :return:
    """
    sbatch_options = ['ntasks', 'cpus_per_task', 'mem', 'gres', 'mail_type',
                      'mail_user']
    file.write("#SBATCH -p {}\n".format(arguments.p))
    file.write("#SBATCH --time {}\n".format(arguments.time))
    for name in sbatch_options:
        if getattr(arguments, name):
            file.write("#SBATCH --{} {}\n".format(name.replace('_', '-'),
                                                  getattr(arguments, name)))
    file.write("\n")


def write_bash(arguments):
    """
    This function writes the bash script that is then handed over to SLURM.

    :param arguments: The argparse arguments
    :return:
    """
    with open('job.sh', 'w', errors='ignore') as file:
        file.write("#!/bin/bash\n")
        write_sbatch(arguments, file)

        if getattr(arguments, 'modules'):
            file.write(
                "# Module initialization and loading modules for "
                "{} \n".format(arguments.modules))
            file.write(". /etc/profile.d/module.sh\n")
            for module in arguments.modules:
                file.write("module load {}\n".format(module))
        file.write("\n")

        if arguments.wrkdir == 'local':
            file.write("# Creates a local working directory.\n"
                       "MYWORKDIR=/usertemp/{}/{}/$SLURM_JOBID\n"
                       "mkdir $MYWORKDIR\n".format(arguments.user_group,
                                                   arguments.user_id))
        else:
            file.write("# Creates a working directory in the BeeGFS"
                       "# network-filesysten\n"
                       "MYWORKDIR=/work/{}/{}/$SLURM_JOBID\n"
                       "mkdir $MYWORKDIR\n".format(arguments.user_group,
                                                   arguments.user_id))
        file.write("\n")

        if getattr(arguments, 'data'):
            file.write("# Copies given files and directories into the\n"
                       "# working directory.\n")
            for path in arguments.data:
                if path == '$SLURM_SUBMIT_DIR/':
                    file.write("cp -r $SLURM_SUBMIT_DIR/{} $MYWORKDIR".format(
                        path))
                else:
                    file.write("cp -r ~/{} $MYWORKDIR\n".format(path))
        file.write("cd $MYWORKDIR\n")
        file.write("\n")

        if getattr(arguments, 'hostfile'):
            file.write("# A host file that holds information about how the job"
                       "# will be distributed\n"
                       "srun -l hostname | awk '{print $2}' | "
                       "sort > hostfile_$SLURM_JOBID\n")
            file.write("\n")

        file.write("# Start calculation.\n"
                   "{}\n\n".format(arguments.run))

        if getattr(arguments, 'save_home'):
            file.write("# Copies given files and directories into the home"
                       "# directory.\n")
            for path in arguments.save_home:
                file.write(
                    "cp -r $MYWORKDIR/{} ~/cluster_result/\n".format(path))
            file.write("\n")
        elif getattr(arguments, 'save_script'):
            file.write("# Copies given files and directories into the "
                       "# directory the script was run from.\n")
            for path in arguments.save_script:
                file.write(
                    "cp -r $MYWORKDIR/{} $SLURM_SUBMIT_DIR/cluster_result"
                    "/\n".format(path))
            file.write("\n")
        else:
            file.write("# Copies all files and directories into the home"
                       "# directory.\n"
                       "cp -r $MYWORKDIR/* ~/cluster_result/\n")

        if not getattr(arguments, 'nocleanup'):
            file.write("# Cleanup\n"
                       "rm -rf $MYWORKDIR\n\n")
        file.write("exit\n")


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__)

    parser.add_argument('run', type=str,
                        help="The command to run. The hostfile can be used "
                             "with 'hostfile_$SLURM_JOBID'. GPU IDs can be "
                             "used with '$GPU_DEVICE_ORDINAL'. "
                             "For example: --run 'abaqus job=Beispiel "
                             "input=Beispiel cpus=8 interactive'")

    sbatch_options = parser.add_argument_group('SBATCH options')
    sbatch_options.add_argument('-p',
                                choices=['ether', 'ib', 'gpu'], default='ether',
                                help="Partition to run the jobs on. Ether uses"
                                " gigabit-ethernet to connect the nodes and is"
                                " used for SMP-calculations. ib uses "
                                "FDR-Infiniband to connect the nodes and is "
                                "used for MPP calculations. gpu offers GPUs to "
                                "run calculations on them.")
    sbatch_options.add_argument('--ntasks', type=int,
                                help="Number of MPI-Processes.", default=1)
    sbatch_options.add_argument('--cpus_per_task', type=int,
                                help="Number of cores per task/MPI-Process.",
                                default=1)
    sbatch_options.add_argument('--mem', type=int,
                                help="Reserves memory in mb.")
    sbatch_options.add_argument('--time', type=str, default='01:00:00',
                                help="Reserves the hardware for the given time"
                                " in hours. This can be chosen generous. Format"
                                " is hours:minutes:seconds")
    sbatch_options.add_argument('--gres', type=str,
                                help="Reserves generic hardware resources. For "
                                "example --gres gpu:1 reserves a gpu in the gpu"
                                " partition")

    email = parser.add_argument_group('Email')
    email.add_argument('--mail_type',
                       choices=['BEGIN', 'END',
                                         'FAIL', 'REQUEUE', 'ALL'],
                       help="Defines if and when an email is sent to "
                       "the user")
    email.add_argument('--mail_user', type=str,
                       help="Email address to use for messages.")

    job_setup = parser.add_argument_group('Job setup')
    job_setup.add_argument('--modules', type=str, nargs='+',
                           help="A list of modules that need to be loaded. "
                                "Specific versions can be loaded. For example: "
                                "--modules 'intel' 'abaqus' "
                                "'gromacs/4.6.5-gpu'")

    job_setup.add_argument('--wrkdir', type=str, choices=['local', 'beegfs'],
                           help="Creates the working directory either locally "
                                "or on the BeeGFS network-filesystem. Needs "
                                "the unix group (--user_group) and user id "
                                "(--user_id).",
                           default='local')
    job_setup.add_argument('--user_group', type=str, required=True,
                           help="Unix group used for working directories.")
    job_setup.add_argument('--user_id', type=str, required=True,
                           help="User id used for working directories.")
    job_setup.add_argument('--data', type=str, nargs='+',
                               help="Copies the given files or directories into"
                               " the working directory. Use $SLURM_SUBMIT_DIR/"
                               " as the directory the bash script was run from.")
    job_setup.add_argument('--hostfile', type=bool,
                           help="Creates a host file that holds information "
                                "about how the job will be distributed. This "
                                "is needed for MPI jobs.")

    post_job = parser.add_argument_group('Post Job')
    post_job.add_argument('--save_home', type=str, nargs='+',
                          help="Files and directories that are saved into the "
                               "home directory. Use '*' to save everything. "
                               "For example: --save_home 'results.txt' "
                               "'resultfolder/data.dat'")
    post_job.add_argument('--save_script', type=str, nargs='+',
                          help="Files and directories that are saved into the "
                               "directory the script was run from. Use '*' "
                               "to save everything. For example: --save_script "
                               "'results.txt' 'resultfolder/data.dat'")
    post_job.add_argument('--nocleanup', type=bool,
                          help="True to not clean up after yourself.")
    args = parser.parse_args()

    if args.mail_type and args.mail_user is None:
        parser.error("--mail_type requires --mail_user.")

    write_bash(args)


if __name__ == '__main__':
    exit(main())
